﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Reflection;

namespace TestLib
{
    public static class TestLib
    {
        public static Assertion<T> Expect<T>(this T obj1)
        {
            return new Assertion<T>(obj1);
        }
    }

    public class Assertion<T>
    {
        private bool invert;
        private T obj1;
        private bool checkProperties;
        private List<string> excludedProperties;

        public Assertion(T avalue, bool ainvert = false)
        {
            obj1 = avalue;
            invert = ainvert;
            excludedProperties = new List<string>();
            checkProperties = false;
        }

        /// <summary>
        /// Checks if object and the given value are equal.
        /// </summary>
        /// <param name="obj2">Value to compare with object.</param>
        public void Eq<T2>(T2 obj2)
        {
            bool retval = true;
            if (!checkProperties)
            {
                if (typeof(T) != typeof(T2))
                    retval = false;
                else
                {
                    var specificEquals = typeof(T).GetMethod("Equals", new Type[] { typeof(T) });
                    if (specificEquals != null && specificEquals.ReturnType == typeof(bool))
                    {
                        retval = (bool)specificEquals.Invoke(obj1, new object[] { obj2 });
                    }
                    else
                        retval = obj1.Equals(obj2);
                }
            }
            else 
            {
                var properties1 = obj1.GetType().GetProperties();
                var properties2 = obj2.GetType().GetProperties();

                if (properties1 == null || properties2 == null)
                    retval = false;

                foreach (var p1 in properties1)
                {
                    if (excludedProperties.FirstOrDefault(s => s == p1.Name) == null)
                    {

                        var p2 = properties2.FirstOrDefault(p => p.Name == p1.Name);
                        if (p2 == null)
                        {
                            retval = false;
                            break;
                        }

                        if (p1.GetValue(obj1, null) != p2.GetValue(obj2, null))
                        {
                            retval = false;
                            break;
                        }
                    }
                }
            }

            if (invert)
                retval = !retval;

            if (!retval)
                throw new ExpectationFailedException();
        }

        /// <summary>
        /// Checks if object is greater than the given value.
        /// </summary>
        /// <param name="obj2">Value to compare with object.</param>
        /// <returns>True if object is greater than given value, false otherwhise.</returns>
        /// <exception cref="ArgumentException"></exception>
        public void IsGreater(T obj2)
        {
            bool retval = false;

            if (!(obj1 is IComparable))
                throw new ArgumentException("Object does not implement IComparable interface.");

            retval = ((IComparable)obj1).CompareTo(obj2) > 0;

            if (invert)
                retval = !retval;
            
            if (!retval)
                throw new ExpectationFailedException();
        }
        /// <summary>
        /// Inverts assertion result.
        /// </summary>
        /// <returns>Inverted assertion result.</returns>
        public Assertion<T> Not()
        {
            invert = !invert;
            return this;
        }

        /// <summary>
        /// Checks if the method throws an exception of type Exception or of the given type.
        /// </summary>
        /// <param name="exceptionType">Type of expected exception.</param>
        /// <returns></returns>
        public void RaiseError(Type exceptionType = null)
        {
            bool retval = false;

            if (obj1 is System.Action)
            {
                try
                {
                    (obj1 as Action).Invoke();
                }
                catch (Exception ex)
                {
                    if (exceptionType == null || ex.GetType() == exceptionType)
                        retval = true;
                }
            }

            if (invert)
                retval = !retval;

            if (!retval)
                throw new ExpectationFailedException();
        }

        /// <summary>
        /// Changes equality checking to properties-based.
        /// </summary>
        /// <returns></returns>
        public Assertion<T> Properties()
        {
            checkProperties = true;
            return this;
        }

        /// <summary>
        /// Changes equality checking to properties-based and enables exclusion of some properties in the check.
        /// </summary>
        /// <returns></returns>
        public Assertion<T> PropertiesWithout( Expression<Func<T, string>> predicate)
        {
            string propertyName = predicate.Body.ToString();
            excludedProperties.Add(propertyName.Substring(propertyName.IndexOf('.') + 1));

            return Properties();
        }
    }

    /// <summary>
    /// Attribute to set exception type that is expected to be thrown by method.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public sealed class ExpectedExceptionAttribute : Attribute
    {
        readonly Type expectedType;

        public ExpectedExceptionAttribute(Type expectedType)
        {
            this.expectedType = expectedType;
        }

        public Type ExpectedType
        {
            get { return expectedType; }
        }
    }

    /// <summary>
    /// Attribute to indicate that the class contains test methods.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = true)]
    public sealed class TestClassAttribute : Attribute
    {
    }

    /// <summary>
    /// Attribute to indicate that the method contains code to be tested.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public sealed class TestMethodAttribute : Attribute
    {
    }

    /// <summary>
    /// Exception type thrown when the expectation of the test was not met.
    /// </summary>
    public class ExpectationFailedException : Exception
    { }
}
