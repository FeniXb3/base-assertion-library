﻿namespace TestRunner
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChooseFileButton = new System.Windows.Forms.Button();
            this.TestMethodsListBox = new System.Windows.Forms.ListBox();
            this.PassedTestsCheckedListBox = new System.Windows.Forms.CheckedListBox();
            this.RunTestsButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ChooseFileButton
            // 
            this.ChooseFileButton.Location = new System.Drawing.Point(13, 13);
            this.ChooseFileButton.Name = "ChooseFileButton";
            this.ChooseFileButton.Size = new System.Drawing.Size(75, 23);
            this.ChooseFileButton.TabIndex = 0;
            this.ChooseFileButton.Text = "Choose file";
            this.ChooseFileButton.UseVisualStyleBackColor = true;
            this.ChooseFileButton.Click += new System.EventHandler(this.ChooseFileButton_Click);
            // 
            // TestMethodsListBox
            // 
            this.TestMethodsListBox.FormattingEnabled = true;
            this.TestMethodsListBox.Location = new System.Drawing.Point(13, 42);
            this.TestMethodsListBox.Name = "TestMethodsListBox";
            this.TestMethodsListBox.Size = new System.Drawing.Size(167, 316);
            this.TestMethodsListBox.TabIndex = 1;
            // 
            // PassedTestsCheckedListBox
            // 
            this.PassedTestsCheckedListBox.Enabled = false;
            this.PassedTestsCheckedListBox.FormattingEnabled = true;
            this.PassedTestsCheckedListBox.Location = new System.Drawing.Point(187, 42);
            this.PassedTestsCheckedListBox.Name = "PassedTestsCheckedListBox";
            this.PassedTestsCheckedListBox.Size = new System.Drawing.Size(168, 319);
            this.PassedTestsCheckedListBox.TabIndex = 2;
            // 
            // RunTestsButton
            // 
            this.RunTestsButton.Location = new System.Drawing.Point(187, 12);
            this.RunTestsButton.Name = "RunTestsButton";
            this.RunTestsButton.Size = new System.Drawing.Size(75, 23);
            this.RunTestsButton.TabIndex = 3;
            this.RunTestsButton.Text = "Run tests";
            this.RunTestsButton.UseVisualStyleBackColor = true;
            this.RunTestsButton.Click += new System.EventHandler(this.RunTestsButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(547, 371);
            this.Controls.Add(this.RunTestsButton);
            this.Controls.Add(this.PassedTestsCheckedListBox);
            this.Controls.Add(this.TestMethodsListBox);
            this.Controls.Add(this.ChooseFileButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ChooseFileButton;
        private System.Windows.Forms.ListBox TestMethodsListBox;
        private System.Windows.Forms.CheckedListBox PassedTestsCheckedListBox;
        private System.Windows.Forms.Button RunTestsButton;
    }
}

