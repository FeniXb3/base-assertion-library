﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using TestLib;

namespace TestRunner
{
    public partial class Form1 : Form
    {
        string fileName;
        Assembly asm;
        List<Type> testTypes;
        List<MethodInfo> testMethods;

        public Form1()
        {
            InitializeComponent();
        }

        private void ChooseFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofDialog = new OpenFileDialog();
            ofDialog.Filter = "dll|*.dll";
            if(ofDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fileName = ofDialog.FileName;

                LoadAssembly(fileName);
                ListMethods();
            }
        }

        private void LoadAssembly(string fileName)
        {
            asm = Assembly.LoadFrom(@fileName);
            testTypes = new List<Type>();
            testMethods = new List<MethodInfo>();
        }

        private void ListMethods()
        {
            Type[] loadedTypes = asm.GetTypes();
            foreach (var t in loadedTypes)
            {
                bool isTestClass = false;
                Attribute[] attributes = System.Attribute.GetCustomAttributes(t);

                foreach (var a in attributes)
                {
                    if (a is TestClassAttribute)
                    {
                        testTypes.Add(t);
                        isTestClass = true;
                        break;
                    }
                }

                if (isTestClass)
                {
                    MethodInfo[] methods = t.GetMethods();

                    foreach (var m in methods)
                    {
                        bool isTestMethod = false;

                        Attribute[] methodAttributes = System.Attribute.GetCustomAttributes(m);

                        foreach (var a in methodAttributes)
                        {
                            if (a is TestMethodAttribute)
                            {
                                isTestMethod = true;
                                break;
                            }
                        }

                        if (isTestMethod)
                        {
                            TestMethodsListBox.Items.Add(m.Name);
                            testMethods.Add(m);
                        }
                    }
                }
            }
        }

        private void RunTestsButton_Click(object sender, EventArgs e)
        {
            PassedTestsCheckedListBox.Items.Clear();

            foreach (var m in testMethods)
            {
                Type classType = m.DeclaringType;
                var o = Activator.CreateInstance(classType);
                
                try
                {
                    m.Invoke(o, null);
                    PassedTestsCheckedListBox.Items.Add(m.Name, true);
                }
                catch(Exception ex)
                {
                    bool isExceptionExpected = false;
                    Attribute[] methodAttributes = System.Attribute.GetCustomAttributes(m);

                    foreach (var a in methodAttributes)
                    {
                        if (a is ExpectedExceptionAttribute)
                        {
                            if(ex.InnerException != null && ex.InnerException.GetType() == (a as ExpectedExceptionAttribute).ExpectedType)
                            {
                                isExceptionExpected = true;
                                break;
                            }
                        }
                    }

                    PassedTestsCheckedListBox.Items.Add(m.Name, isExceptionExpected);
                }
            }
        }

    }
}
